import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Index from '@/components/Index/Index'
import Item from  '@/components/Item/Item'
import Result from  '@/components/Result/Result'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },{
      path:'/hello',
      name: 'Hello',
      component: HelloWorld
    },{
      path:'/item',
      name: 'Item',
      component: Item
    },{
      path:'/result',
      name:'Result',
      component: Result
    }
  ]
})
